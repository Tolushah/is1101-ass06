/*Index no :- 19020325
Harindi.M.A.T.
Assignment 6 - Question 2(Fibonacci)*/



#include <stdio.h>

//program to print a fibonacci sequence

//fibonacci sequence
void fibonacciseq(int n);
//fibonacci value
int fibonaccivalue(int n);

 int fibonaccivalue(int n){ 
 	if (n==0) return 0;
 	else if(n==1) return 1;
 	else  return fibonaccivalue(n-1)+ fibonaccivalue(n-2);
 }
 
 void fibonacciseq(int n){
 	
 	
 	if(n>=0){
		 fibonacciseq(n-1);
		 printf("%d\n",fibonaccivalue(n));	
	 }
 }
 int main(){

    int x;
 	printf("Enter a number: ");
 	scanf("%d",&x);
    fibonacciseq(x);
    
 	return 0;
 }
