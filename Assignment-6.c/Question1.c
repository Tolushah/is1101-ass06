/*Index no :- 19020325
Harindi.M.A.T.
Assignment 6 - Question 1(Number pattern)*/


#include <stdio.h>

//program to print a number triangle

// pattern  function
void printrow(int n);
// rows function
void pattern(int n);

//print the number pattern in a row
void printrow(int n){
		
	if(n>0) {
	   printf("%d",n);
	   printrow(n-1);	   
    }
}

//print a new line or given number of rows
void pattern(int n){
	
	if (n>0){
     	pattern(n-1);   //recursively print the pattern
		printrow(n);   //reduce the n recursively.
		printf("\n");  //for new line
}
}

int main(){
	
	int p;
	printf("Enter the no of rows: ");  
	scanf("%d",&p);
	pattern(p);
    
	return 0;
}
